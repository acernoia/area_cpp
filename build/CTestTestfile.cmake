# CMake generated Testfile for 
# Source directory: /home/andreacernoia/Scrivania/area_cpp
# Build directory: /home/andreacernoia/Scrivania/area_cpp/build
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(primo_test "test_rect" "1" "1")
SET_TESTS_PROPERTIES(primo_test PROPERTIES  PASS_REGULAR_EXPRESSION "1")
ADD_TEST(secondo_test "test_rect" "1" "2")
SET_TESTS_PROPERTIES(secondo_test PROPERTIES  PASS_REGULAR_EXPRESSION "2")
ADD_TEST(terzo_test "test_rect" "1" "3")
SET_TESTS_PROPERTIES(terzo_test PROPERTIES  PASS_REGULAR_EXPRESSION "3")
