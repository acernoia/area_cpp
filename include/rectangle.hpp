#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include <string>

using namespace std;

class rectangle{
  int base, altezza;
 public:
  int area();
  void set_values(int,int);
  rectangle(int base, int altezza) : base(base), altezza(altezza){}
};

#endif
